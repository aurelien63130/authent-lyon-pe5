<?php
try {
    $host = 'database';
    $dbName = 'authent_pe5';
    $user = 'root';
    $password = 'tiger';
    $bdd = new PDO(
        'mysql:host='.$host.';dbname='.$dbName.';charset=utf8',
        $user,
        $password);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $e){
    throw new InvalidArgumentException('Erreur connexion à la base de données :'.$e->getMessage());
    exit;
}
?>